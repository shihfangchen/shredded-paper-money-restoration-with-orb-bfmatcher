//exe ori seg

#include <iostream>
#include <opencv2/opencv.hpp>
#include <omp.h>
//#include "opencv2/xfeatures2d.hpp"
#include <map>
#include <unordered_map>
#include <numeric>      // std::accumulate
#include <algorithm>    // std::max 
#include <cmath>    //
#include <experimental/filesystem> // or #include <filesystem>

using namespace std;
using namespace cv;

String num2str(const int num)
{
	stringstream ss;
	ss << num;
	return ss.str();
}


std::tuple< int, int, int, int>  kmeans_reduced_keypoints(vector<Point2f> points)
{
	Mat labels;
	std::vector<Point2f> centers;

	double compactness = kmeans(points, 2, labels,
		TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 10, 1.0),
		3, KMEANS_PP_CENTERS, centers);

	int label0acc = 0, large = 1;

	for (auto i = 1; i < labels.rows; ++i)
	{

		int label = labels.at<int>(i);
		if (label == 0)
		{
			label0acc++;
			if (label0acc > labels.rows / 2)
			{
				large = 0;
				break;
			}
		}
	}

	//cout << "large " << large << endl;
	float rightimg_x_leftmost = INT_MAX, rightimg_x_rightmost = -1, rightimg_y_topmost = INT_MAX, rightimg_x_bottommost = -1;

	for (auto i = 1; i < labels.rows; ++i)
	{
		//cout << "\n" << points[i] << " " << labels.at<int>(i) << endl;
		int label = labels.at<int>(i);
		if (label == large)
		{
			rightimg_x_leftmost = min(rightimg_x_leftmost, points[i].x) - 1;
			rightimg_y_topmost = min(rightimg_y_topmost, points[i].y) - 1;
			rightimg_x_rightmost = max(rightimg_x_rightmost, points[i].x) + 1;
			rightimg_x_bottommost = max(rightimg_x_bottommost, points[i].y) + 1;
		}
	}

	return std::make_tuple(rightimg_x_leftmost, rightimg_y_topmost, rightimg_x_rightmost, rightimg_x_bottommost);

}

//#pragma omp parallel
int main(int argc, char ** argv)
{

	//******************** speed-up using multithreads vvv
	setUseOptimized(true);
	std::ios_base::sync_with_stdio(false);
	cin.tie(0);
	//******************** speed-up using multithreads

	namespace fs = std::experimental::filesystem;
	if (fs::is_directory("false") || fs::exists("false"))
	{
		std::experimental::filesystem::remove_all("false");
	}
	fs::create_directory("false"); // create src folder

	bool show_and_draw = 1;

	Mat img_1;

	vector<cv::String> fn;
	string folder_path = argv[1];
	string image_path = folder_path.append("/*.bmp");
	glob(image_path, fn, false);

	vector <Mat> Slice_and_Erode_countour;
	size_t count = fn.size(); //number of png files in images folder
	for (size_t i = 0; i < count; i++)
	{
		Slice_and_Erode_countour.push_back(imread(fn[i], 0));
		//imshow("i", Slice_and_Erode_countour[i]);
		//waitKey();
	}

	//system("pause");
	//cvtColor(img_1, _img_1_luv, COLOR_BGR2Luv);

	Mat img_2 = imread(argv[2], 0);
	Mat img_matches;
	Mat img_test;


	bool kp2_cal = 0, outcanvas = 0;
	static Mat merge, merge_thres;

	//int Slice_and_Erode_countour_size = Slice_and_Erode_countour.size();
	int Slice_and_Erode_countour_idx = 0;

	int fastThreshold = 5;
	float scale_ratio = 1.2;

	cout << "show_and_draw :";
	cin >> show_and_draw;

	cout << "scale_ratio :";
	cin >> scale_ratio;

	cout << "fastThreshold :";
	cin >> fastThreshold;

	double total_cost = (double)getTickCount();

	Mat OutImage;
	for (auto Slice_and_Erode_countour_img : Slice_and_Erode_countour)
	{
		double slice_cost = (double)getTickCount();

		Slice_and_Erode_countour_idx++;
		cout << "\n" << Slice_and_Erode_countour_idx << " / " << count << "\n";

		img_1 = Slice_and_Erode_countour_img;
		//Mat img_1_slide_contour_erode = Slice_and_Erode_countour_img;

		if (show_and_draw)
		{
			cv::imshow("Slice_and_Erode_countour_img", img_1);
			//cv::imwrite("Slice_and_Erode_countour_img.bmp", img_1);
			//cv::imwrite("Slice_and_Erode_countour_img.bmp", img_1);
		}

		//**************************************************************************
		//******* slice_image_keypoints_and_descriptors_finding_and_reducing *******
		//**************************************************************************

		//cv::imwrite("img_1_slide_contour_erode.bmp", img_1_slide_contour_erode);

		std::vector<KeyPoint> keypoints_1, keypoints_2;

		Mat descriptors_1, descriptors_2;

		//cv::Ptr<Feature2D> f2d = xfeatures2d::SIFT::create();
		// Initiate ORB detector
		cv::Ptr<Feature2D> f2d = ORB::create(
			img_1.total() *0.8, //nfeatures
			1.05f, //scaleFactor
			16,  //nlevels
			11, //edgeThreshold
			1, //firstLevel
			2, //WTA_K
			ORB::HARRIS_SCORE, //scoreType
			21, //patchSize
			fastThreshold //fastThreshold
		);
		f2d->detect(img_1, keypoints_1);

		if (keypoints_1.size() <= 5)
		{
			imwrite("false//" + num2str(Slice_and_Erode_countour_idx) + ".bmp", img_1);
			continue;
		}

		//Mat tmp1;
		//if (show_and_draw)
		//{
		//	drawKeypoints(img_1, keypoints_1, tmp1);

		//	//cout << "0 keypoints_1.size() " << keypoints_1.size() << "\n" << "\n";
		//	//cv::imshow("keypoints_1", tmp1);

		//	cv::imwrite("0 keypoints_1.bmp", tmp1);
		//}

		int img_1_maks_z = img_1.total() - countNonZero(img_1);
		//cout << "\n" << "img_1_maks_z " << img_1_maks_z<<"\n";
		//waitKey();
		Mat imageEroded;
		//int kSize = img_1_maks_z * 0.005;
		//if (!(kSize & 1))
		//	kSize--;
		int kSize = 3;

		Mat element = getStructuringElement(0, Size(kSize, kSize));
		Mat img_1_erode;
		img_1.copyTo(img_1_erode);
		threshold(img_1_erode, img_1_erode, 0, 255, THRESH_BINARY);

		morphologyEx(img_1_erode, img_1_erode, MORPH_ERODE, element);

		std::vector<KeyPoint> reduced_keypoints_1;

		for (auto it = keypoints_1.begin(); it != keypoints_1.end(); it++)
		{
			static int index;

			index = it - keypoints_1.begin();

			if (int(img_1_erode.at<uchar>(keypoints_1[index].pt) == 255))
				reduced_keypoints_1.push_back(keypoints_1[index]);
		}


		if (reduced_keypoints_1.size() <= 10)
		{
			cout << "\n" << "reduced_keypoints_1.size() " << reduced_keypoints_1.size() << "\n";
			imwrite("false//" + num2str(Slice_and_Erode_countour_idx) + ".bmp", img_1);
			continue;
		}

		keypoints_1 = reduced_keypoints_1;

		f2d->compute(img_1, keypoints_1, descriptors_1);
		//if (show_and_draw)
		//{
		//	drawKeypoints(img_1, keypoints_1, tmp1);

		//	//cout << "0 2 keypoints_1.size() " << keypoints_1.size() << "\n" << "\n";
		//	cv::imwrite("0 2 keypoints_1.bmp", tmp1);
		//}

		//cv::imshow("keypoints_1_reduce", tmp1);

//*****************************************************************************
//******* template_image_keypoints_and_descriptors_finding_and_reducing *******
//*****************************************************************************
		static vector<KeyPoint> save_keypoints_2;

		static Mat save_descriptors_2;

		if (!kp2_cal)
		{
			//cout << "\n" << "kp2_cal" << "\n";
			// Initiate ORB detector
			f2d = ORB::create(
				img_2.total() *0.8, //nfeatures
				1.05f, //scaleFactor
				16,  //nlevels
				11, //edgeThreshold
				1, //firstLevel
				2, //WTA_K
				ORB::HARRIS_SCORE, //scoreType
				21, //patchSize
				fastThreshold //fastThreshold
			);
			f2d->detectAndCompute(img_2, Mat(), keypoints_2, descriptors_2);
			save_keypoints_2 = keypoints_2;
			save_descriptors_2 = descriptors_2;
			kp2_cal = 1;
		}
		else
		{
			keypoints_2 = save_keypoints_2;
			descriptors_2 = save_descriptors_2;
		}

		//double BFMatcher_cost = (double)getTickCount();

//*****************************************************************************
//*******-- Step 3: Matching descriptor vectors using BFMatcher :**************
//*******One of NORM_L1, NORM_L2, NORM_HAMMING, NORM_HAMMING2.L1 and***********
//*******L2 norms are preferable choices for SIFT and SURF descriptors,********
//*******NORM_HAMMING should be used with ORB, BRISK and BRIEF, ***************
//*******NORM_HAMMING2 should be used with ORB when WTA_K == 3 or 4 ***********
//*****************************************************************************

		BFMatcher matcher(NORM_HAMMING); //NORM_L1  //NORM_HAMMING
		std::vector< DMatch > matches;

		try
		{
			matcher.match(descriptors_1, descriptors_2, matches);
		}
		catch (...)
		{
			imwrite("false//" + num2str(Slice_and_Erode_countour_idx) + ".bmp", img_1);
			continue;
		}
		//cout << "\n" <<"BFMatcher_cost "<< ((double)getTickCount() - BFMatcher_cost) / getTickFrequency()<< "\n";

		drawMatches(img_1, keypoints_1, img_2, keypoints_2,
			matches, img_matches, Scalar::all(-1), Scalar::all(-1),
			std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		//Mat tmp2;

		//		if (show_and_draw)
		//		{
		//			cv::imwrite("1 BFMatcher.bmp", img_matches);
		//
		//			Mat tmp_tmp2;
		//			tmp2.copyTo(tmp_tmp2);
		//			drawKeypoints(img_2, keypoints_2, tmp_tmp2);
		//
		//			tmp_tmp2.copyTo(tmp2);
		//			//cout << "keypoints_2.size() " << keypoints_2.size() << "\n" << "\n";
		//			cv::imwrite("keypoints_2.bmp", tmp2);
		//		}

//*********************************************************************************
//******* Filter matches using the Lowe's ratio test with standardDeviation *******
//*********************************************************************************

		int sum_matches_distance = 0, avg_sum_matches_distance = 0;
		for (int i = 0; i < descriptors_1.rows; i++)
		{
			sum_matches_distance += matches[i].distance;
		}
		avg_sum_matches_distance = sum_matches_distance / descriptors_1.rows;

		float standardDeviation = 0.0;
		for (int i = 0; i < descriptors_1.rows; ++i)
			standardDeviation += pow(matches[i].distance - avg_sum_matches_distance, 2);

		std::vector< DMatch > good_matches;
		for (int i = 0; i < descriptors_1.rows; i++)
		{
			if (matches[i].distance < avg_sum_matches_distance + standardDeviation)
				good_matches.push_back(matches[i]);
		}

		//if (show_and_draw)
		//{
		//	Mat tmp_img_matches;
		//	img_matches.copyTo(tmp_img_matches);

		//	drawMatches(img_1, keypoints_1, img_2, keypoints_2,
		//		good_matches, tmp_img_matches, Scalar::all(-1), Scalar::all(-1),
		//		std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

		//	tmp_img_matches.copyTo(img_matches);

		//	cv::imwrite("2 Lowe's  BFMatcher.bmp", img_matches);
		//}

		//cv::imshow("Good Matches", img_matches);

		vector<DMatch>& Input_matches = good_matches; // good_matches, matches
		// get feat point
		vector<Point2f> featPoint1;
		vector<Point2f> featPoint2;

		std::vector<KeyPoint> keypoints_1_n, keypoints_2_n;

		for (size_t i = 0; i < Input_matches.size(); i++)
		{
			//-- Get the keypoints from the good matches
			featPoint1.push_back(keypoints_1[Input_matches[i].queryIdx].pt);
			featPoint2.push_back(keypoints_2[Input_matches[i].trainIdx].pt);
			//cout << "\n" << featPoint1[i] << "\n";
			//cout << "\n" << featPoint2[i] << "\n";
		}

		if (Input_matches.size() > 10)
		{
			vector<Point2f> m_LeftInlier;
			vector<Point2f> m_RightInlier;
			vector<DMatch> m_InlierMatches;
			vector<char> m_RANSACStatus;

			m_RANSACStatus.resize(0);
			m_RANSACStatus.clear();

			int InlinerCount = 0, OutlinerCount = 0;

			//****************************************************************************************************
			//******* reducong outliers by RANSAC ****************************************************************
			//******* improved RANSAC insufficient features by tuning findHomography ransacReprojThreshold *******
			//****************************************************************************************************
			for (int ransacReprojThreshold = 5; ransacReprojThreshold <= 40; ransacReprojThreshold *= 2)
			{
				//cout << "\n" << "ransacReprojThreshold " << ransacReprojThreshold << "\n";
				findHomography(featPoint1, featPoint2, FM_RANSAC, ransacReprojThreshold, m_RANSACStatus, 6000, 0.995);

				OutlinerCount = 0;
				for (int i = 0; i < Input_matches.size(); i++)
				{
					if (m_RANSACStatus[i] == 0)
					{
						OutlinerCount++;
					}
				}

				InlinerCount = Input_matches.size() - OutlinerCount;
				//cout << "\n" << "InlinerCount " << InlinerCount << "\n";

				static bool setv = 1;
				static int previous_InlinerCount;
				static vector<char> previous_m_RANSACStatus;

				if (ransacReprojThreshold == 40 && InlinerCount < 40)
				{
					cout << "\n" << ".........................." << "\n";
					break;
				}
				else if (ransacReprojThreshold == 40 && InlinerCount >= 40 && setv == 1)
				{
					ransacReprojThreshold = 10;
					previous_InlinerCount = InlinerCount;
					previous_m_RANSACStatus = m_RANSACStatus;
					setv = 0;
				}
				else if (ransacReprojThreshold == 20 && InlinerCount < 40 && setv == 1)
				{
					InlinerCount = previous_InlinerCount;
					m_RANSACStatus = previous_m_RANSACStatus;
					cout << "\n" << "xxxxx" << "\n";
					break;
				}

				if (setv)
					if (InlinerCount >= 40)
						ransacReprojThreshold *= 4;
					else
						ransacReprojThreshold = 20;
			}

			//cout << "\n" << "InlinerCount " << InlinerCount << "\n";
			if (InlinerCount <= 40)
			{
				imwrite("false//" + num2str(Slice_and_Erode_countour_idx) + ".bmp", img_1);
				continue;
			}

			m_LeftInlier.clear();
			m_RightInlier.clear();
			m_InlierMatches.clear();
			m_InlierMatches.resize(InlinerCount);
			m_LeftInlier.resize(InlinerCount);
			m_RightInlier.resize(InlinerCount);
			InlinerCount = 0;

			for (int i = 0; i < Input_matches.size(); i++)
			{
				if (m_RANSACStatus[i] != 0)
				{
					m_LeftInlier[InlinerCount].x = featPoint1[i].x;
					m_LeftInlier[InlinerCount].y = featPoint1[i].y;
					m_RightInlier[InlinerCount].x = featPoint2[i].x;
					m_RightInlier[InlinerCount].y = featPoint2[i].y;
					m_InlierMatches[InlinerCount].queryIdx = InlinerCount;
					m_InlierMatches[InlinerCount].trainIdx = InlinerCount;
					InlinerCount++;
				}
			}


			if (m_LeftInlier.size() > 10 || m_RightInlier.size() > 10)
			{
				//**********************************************************************************************************************************************************
				//******* using_kmeans_reduced_outlier_keypoints by filtering large cluster's leftimg/rightimg leftmost/rightmost/topmost/bottommost ***********************
				//**********************************************************************************************************************************************************

				float rightimg_x_leftmost = INT_MAX, rightimg_x_rightmost = -1, rightimg_y_topmost = INT_MAX, rightimg_x_bottommost = -1;

				std::tie(rightimg_x_leftmost, rightimg_y_topmost, rightimg_x_rightmost, rightimg_x_bottommost)
					= kmeans_reduced_keypoints(m_RightInlier);

				float leftimg_x_leftmost = INT_MAX, leftimg_x_rightmost = -1, leftimg_y_topmost = INT_MAX, leftimg_x_bottommost = -1;

				std::tie(leftimg_x_leftmost, leftimg_y_topmost, leftimg_x_rightmost, leftimg_x_bottommost)
					= kmeans_reduced_keypoints(m_LeftInlier);

				InlinerCount = Input_matches.size() - OutlinerCount;
				m_InlierMatches.resize(InlinerCount);
				m_LeftInlier.resize(InlinerCount);
				m_RightInlier.resize(InlinerCount);

				vector<Point2f> ori_m_RightInlier = m_RightInlier;

				static vector<Point2f> pre_m_LeftInlier;
				static vector<Point2f> pre_m_RightInlier;
				int pre_InlinerCount = InlinerCount;

				InlinerCount = 0;
				pre_m_LeftInlier.clear();
				pre_m_RightInlier.clear();

				pre_m_LeftInlier = m_LeftInlier;
				pre_m_RightInlier = m_RightInlier;

				for (int i = 0; i < Input_matches.size(); i++)
				{

					if (m_RANSACStatus[i] != 0
						&& featPoint2[i].x > rightimg_x_leftmost
						&& featPoint2[i].x < rightimg_x_rightmost
						&& featPoint2[i].y > rightimg_y_topmost
						&& featPoint2[i].y < rightimg_x_bottommost

						&& featPoint1[i].x > leftimg_x_leftmost
						&& featPoint1[i].x < leftimg_x_rightmost
						&& featPoint1[i].y > leftimg_y_topmost
						&& featPoint1[i].y < leftimg_x_bottommost
						)
					{
						m_LeftInlier[InlinerCount].x = featPoint1[i].x;
						m_LeftInlier[InlinerCount].y = featPoint1[i].y;
						m_RightInlier[InlinerCount].x = featPoint2[i].x;
						m_RightInlier[InlinerCount].y = featPoint2[i].y;
						m_InlierMatches[InlinerCount].queryIdx = InlinerCount;
						m_InlierMatches[InlinerCount].trainIdx = InlinerCount;
						InlinerCount++;

					}
				}

				if (InlinerCount < 10)
				{
					InlinerCount = pre_InlinerCount;
					m_LeftInlier = pre_m_LeftInlier;
					m_RightInlier = pre_m_RightInlier;
				}
				else
				{
					m_InlierMatches.resize(InlinerCount);
					m_LeftInlier.resize(InlinerCount);
					m_RightInlier.resize(InlinerCount);
				}

				vector<KeyPoint> key1(InlinerCount);
				vector<KeyPoint> key2(InlinerCount);
				KeyPoint::convert(m_LeftInlier, key1);
				KeyPoint::convert(m_RightInlier, key2);
				drawMatches(img_1, key1, img_2, key2, m_InlierMatches, OutImage);

				//****************************************************************
				//******* using_meanStdDev_reduce_outliers ***********************
				//****************************************************************

				Scalar mean, stddev;
				vector<Point2f> tmp_m_RightInlier;
				vector<Point2f> tmp_m_LeftInlier;

				tmp_m_RightInlier.clear();
				tmp_m_LeftInlier.clear();


				int SD_tune_kpt = 1;
				meanStdDev(m_RightInlier, mean, stddev);

				for (auto i : m_RightInlier)
				{
					if (m_RightInlier.size() < 10)
					{

						if (show_and_draw)
						{
							cv::putText(OutImage, //target image
								"Not enough matches", //text
								cv::Point(OutImage.cols / 3, OutImage.rows / 2), //top-left position
								cv::FONT_HERSHEY_DUPLEX,
								1.0,
								Scalar(0, 0, 255), //font color
								2);
							circle(OutImage, Point(img_1.cols + mean[0], mean[1]), 20, Scalar(0, 0, 255), 1);
							namedWindow("findFundamentalMat RANSAC Matches", WINDOW_NORMAL);
							cv::imshow("findFundamentalMat RANSAC Matches", OutImage);
							cv::imwrite("findFundamentalMat 3 RANSAC Matches.bmp", OutImage);
						}
						break;
					}

					if (stddev[0] >= 10 && stddev[1] >= 10)
					{

						tmp_m_RightInlier.clear();
						tmp_m_LeftInlier.clear();
						for (auto i = 0; i < m_RightInlier.size(); i++)
						{
							static float SD_ratio = 1.5;
							if (
								m_RightInlier[i].x > mean[0] - SD_ratio * stddev[0]
								&& m_RightInlier[i].x < mean[0] + SD_ratio * stddev[0]
								&& m_RightInlier[i].y > mean[1] - SD_ratio * stddev[1]
								&& m_RightInlier[i].y < mean[1] + SD_ratio * stddev[1]
								)
							{
								tmp_m_RightInlier.push_back(m_RightInlier[i]);
								tmp_m_LeftInlier.push_back(m_LeftInlier[i]);
							}
						}
						meanStdDev(tmp_m_RightInlier, mean, stddev);
						SD_tune_kpt++;
					}

					if (SD_tune_kpt == 3 || (stddev[0] <= 10 || stddev[1] <= 10))
					{
						int h_empty = 0;
						if (SD_tune_kpt > 0)
						{
							Mat h = findHomography(m_LeftInlier, m_RightInlier, RANSAC);
							if (h.elemSize() == 0)
							{
								h_empty = 1;
							}
						}

						if (tmp_m_LeftInlier.size() >= 10 && tmp_m_RightInlier.size() >= 10 && h_empty == 0)
						{
							m_RightInlier = tmp_m_RightInlier;
							m_LeftInlier = tmp_m_LeftInlier;
						}
						break;
					}

				}

				Mat colour_img_1 = img_1;
				cvtColor(colour_img_1, colour_img_1, COLOR_GRAY2BGR);

				if (m_RightInlier.size() >= 10 || m_LeftInlier.size() >= 10)
				{
					if (show_and_draw)
					{
						circle(OutImage, Point(img_1.cols + mean[0], mean[1]), 20, Scalar(0, 255, 0), 1);
						try
						{
							colour_img_1.copyTo(OutImage(Rect(0, colour_img_1.rows + 10, colour_img_1.cols, colour_img_1.rows)));
						}
						catch (...) {}
					}

					//***************************************************************
					//******* Calculate the homography ******************************
					//***************************************************************

					Mat h = findHomography(m_LeftInlier, m_RightInlier, RANSAC);
					//cout << "\n" << "h " << h << "\n";
					Size size(img_2.cols, img_2.rows);
					Mat im_dst = Mat::zeros(size, CV_8UC3);
					warpPerspective(img_1, im_dst, h, size, INTER_NEAREST);

					if (show_and_draw)
					{
						namedWindow("im_dst", WINDOW_NORMAL);
						cv::imshow("im_dst", im_dst);
					}

					//**************************************************************************************************************************************
					//******* decomposeHomographyMat to find rotation matrices / translation matrices / plane normal matrices ******************************
					//**************************************************************************************************************************************

					// set up a virtual camera
					float f = 1, w = img_1.cols, height = img_1.rows;

					cv::Mat1f K = (cv::Mat1f(3, 3) <<
						f, 0, w / 2,
						0, f, height / 2,
						0, 0, 1);

					// decompose using identity as internal parameters matrix
					std::vector<cv::Mat> Rs, Ts, plane_normal_matrices;
					cv::Mat tempt;
					cv::Mat U, W, V;
					float Scale_factor;
					if (h.empty())
					{
						cout << "\n" << ".........................." << "\n";
						imwrite("false//" + num2str(Slice_and_Erode_countour_idx) + ".bmp", img_1);
						continue;
					}

					cv::decomposeHomographyMat(h,
						K,
						Rs, Ts,
						plane_normal_matrices);

					h.convertTo(tempt, CV_32FC1);

					//***************************************************************
					//******* SVD to find Scale_factor ******************************
					//***************************************************************

					cv::SVD::compute(tempt, W, U, V, SVD::MODIFY_A);
					Scale_factor = 1.0 / W.row(1).at<float>(0);

					//double countNonZero_im_dst = countNonZero(im_dst), countNonZero_img_1 = countNonZero(img_1);

					//double countNonZero_ratio = countNonZero_im_dst / countNonZero_img_1;

//***************************************************************************************************
//******* calculate to shape distance betewwn slice and warpping slice ******************************
//***************************************************************************************************

					double matchShapes_distance = matchShapes(img_1, im_dst, CONTOURS_MATCH_I2, 0);

					if (show_and_draw)
					{
						cout << "\n" << "Scale " << Scale_factor << "\n";
						cout << "\n" << "matchShapes_distance " << matchShapes_distance << "\n";
						//cout << "countNonZero_ratio " << countNonZero_ratio << "\n";

						//cout << "W " << "\n" << W << "\n";
						//cout << "U " << "\n" << U << "\n";
						//cout << "V " << "\n" << V << "\n";
					}

					//*****************************************************************************************************************************************
					//******* defining sparse / distort warpAffine result by combining matchShapes_distance and SVD Scale_factor ******************************
					//*****************************************************************************************************************************************

					if (matchShapes_distance >= 0.08 || Scale_factor >= scale_ratio * 1.5 || Scale_factor > scale_ratio * 1.3 || Scale_factor < scale_ratio * 0.5)
					{
						if (show_and_draw)
						{
							cv::putText(OutImage, //target image
								"sparse", //text
								cv::Point(OutImage.cols / 3, OutImage.rows / 2), //top-left position
								cv::FONT_HERSHEY_DUPLEX,
								1.0,
								Scalar(0, 0, 255), //font color
								2);
						}
						imwrite("false//" + num2str(Slice_and_Erode_countour_idx) + ".bmp", img_1);
					}
					else
					{
						if (Scale_factor >= scale_ratio * 1.2 || Scale_factor <= scale_ratio * 0.8)
						{
							cv::putText(OutImage, //target image
								"distortion", //text
								cv::Point(OutImage.cols / 3, OutImage.rows / 2), //top-left position
								cv::FONT_HERSHEY_DUPLEX,
								1.0,
								Scalar(0, 0, 255), //font color
								2);

							//cout << "-------------------------------------------\n";
							//cout << "Estimated decomposition:\n\n";

							//cv::Mat1d rvec;
							//cv::Rodrigues(Rs[0], rvec);
							//Mat angle_mat = (rvec * 180 / CV_PI).row(0);
							//double angle00 = abs(angle_mat.at<double>(0));

							//angle_mat = (rvec * 180 / CV_PI).row(1);
							//double angle01 = abs(angle_mat.at<double>(0));

							//double angle0_01 = angle00 + angle01;

							//cv::Mat1d pre_rvec = rvec;

							//cv::Rodrigues(Rs[2], rvec);
							//angle_mat = (rvec * 180 / CV_PI).row(0);
							//double angle20 = abs(angle_mat.at<double>(0));

							//angle_mat = (rvec * 180 / CV_PI).row(1);
							//double angle21 = abs(angle_mat.at<double>(0));
							//double angle2_01 = angle20 + angle21;

							//if (angle0_01 < angle2_01)
							//	cv::Rodrigues(Rs[0], rvec);
							//else
							//	cv::Rodrigues(Rs[2], rvec);

							//angle_mat = (rvec * 180 / CV_PI).row(2);

							//double angle = 360 - angle_mat.at<double>(0);

							//Mat angle_mat0 = (rvec * 180 / CV_PI).row(0), angle_mat1 = (rvec * 180 / CV_PI).row(1);
							//if (angle_mat0.at<double>(0) <= -1 && angle_mat1.at<double>(0) <= 0)
							//	angle = -angle;

							//double Ts_x = abs(Ts[0].row(0).at<double>(0));
							//double Ts_y = abs(Ts[0].row(0).at<double>(1));

							//if (show_and_draw)
							//{
							//	cout << "rvec = " << std::endl;
							//	for (auto R_ : Rs)
							//	{
							//		cv::Mat1d rvec;
							//		cv::Rodrigues(R_, rvec);
							//		cout << rvec * 180 / CV_PI << std::endl << std::endl;
							//	}

							//	cout << "\n" << "t = " << "\n";
							//	for (auto t_ : Ts)
							//	{
							//		cout << t_ << std::endl << std::endl;
							//	}
							//	cout << "\n" << "n = " << "\n";


							//	cout << "\n" << "Ts_x " << Ts_x << "\n";
							//	cout << "\n" << "Ts_y " << Ts_y << "\n" << "\n";

							//	for (auto n_ : plane_normal_matrices)
							//	{
							//		cout << n_ << std::endl << std::endl;
							//	}cv::cv::imwrite
							//}

							//Mat im_dst_tmp = Mat::zeros(size * 2, CV_8UC1);

							//try {
							//	img_1.copyTo(im_dst_tmp(Rect(Ts_x, Ts_y, img_1.cols, img_1.rows)));
							//}
							//catch (...) { system("pause"); }

							//cv::Point2f center((img_1.cols - 1) / 2.0 + Ts_x, (img_1.rows - 1) / 2.0 + Ts_y);

							//Mat rot_mat, rotated_im;
							//rot_mat = getRotationMatrix2D(center, angle, 1);
							//warpAffine(im_dst_tmp, rotated_im, rot_mat, im_dst_tmp.size());

							//rotated_im = rotated_im(Range(0, rotated_im.rows / 2), Range(0, rotated_im.cols / 2));
							//
							//circle(rotated_im, center, 20, Scalar(255, 255, 255), 1);
							//im_dst = rotated_im;

							//namedWindow("rotated_im", WINDOW_NORMAL);
							//cv::imshow("rotated_im", rotated_im);
							//cv::imwrite("rotated_im.bmp", rotated_im);

							//waitKey();
						}

						//***********************************************************************************************
						//******* merge warpAffine image ****************************************************************
						//******* threshlod warpAffine image to 0 and 1 to create a heatmap table ***********************
						//******* applyColorMap by normalize heatmap table **********************************************
						//***********************************************************************************************

						if (merge.empty())
							merge = im_dst;
						else
						{
							Mat merge_thres;
							threshold(merge, merge_thres, 0, 255, THRESH_BINARY);
							add(merge, im_dst, merge, ~merge_thres);
						}

						Mat im_dst_thres;

						threshold(im_dst, im_dst_thres, 0, 1, THRESH_BINARY);

						if (merge_thres.empty())
							merge_thres = im_dst_thres;
						else
							merge_thres += im_dst_thres;

						if (show_and_draw)
						{
							namedWindow("merge", WINDOW_NORMAL);
							cv::imshow("merge", merge);
							cv::imwrite("merge.bmp", merge);

							// Holds the colormap version of the image:
							Mat merge_thres_colormap, tmp_merge_thres;
							// Apply the colormap:
							cv::normalize(merge_thres, tmp_merge_thres, 0, 255, cv::NORM_MINMAX);
							applyColorMap(tmp_merge_thres, merge_thres_colormap, COLORMAP_JET);
							// Show the result:
							cv::namedWindow("merge_thres_colormap", WINDOW_NORMAL);
							cv::imshow("merge_thres_colormap", merge_thres_colormap);
						}

					}

					if (show_and_draw)
					{
						namedWindow("findFundamentalMat RANSAC Matches", WINDOW_NORMAL);
						cv::imshow("findFundamentalMat RANSAC Matches", OutImage);
						cv::imwrite("findFundamentalMat 3 RANSAC Matches.bmp", OutImage);
					}

				}

			}
			else
			{
				vector<KeyPoint> key1(InlinerCount);
				vector<KeyPoint> key2(InlinerCount);
				if (show_and_draw)
				{
					KeyPoint::convert(m_LeftInlier, key1);
					KeyPoint::convert(m_RightInlier, key2);
					drawMatches(img_1, key1, img_2, key2, m_InlierMatches, OutImage);

					cv::putText(OutImage, //target image
						"Not enough matches", //text
						cv::Point(OutImage.cols / 3, OutImage.rows / 2), //top-left position
						cv::FONT_HERSHEY_DUPLEX,
						1.0,
						Scalar(0, 0, 255), //font color
						2);

					namedWindow("findFundamentalMat RANSAC Matches", WINDOW_NORMAL);
					cv::imshow("findFundamentalMat RANSAC Matches", OutImage);
					cv::imwrite("findFundamentalMat 3 RANSAC Matches.bmp", OutImage);
				}
			}

		}
		else
		{
			//cout << "Not enough matches are found - ";
			imwrite("false//" + num2str(Slice_and_Erode_countour_idx) + ".bmp", img_1);
			continue;
		}

		if (show_and_draw)
			waitKey(1);
		//system("pause");
		cout << "\n" << "slice_cost " << ((double)getTickCount() - slice_cost) / getTickFrequency() << "\n";

	}

	cout << "\n" << "total_cost " << ((double)getTickCount() - total_cost) / getTickFrequency() << "\n";

	if (!merge.empty())
	{
		namedWindow("F merge", WINDOW_NORMAL);
		cv::imshow("F merge", merge);
		cv::imwrite("F merge.bmp", merge);

		//***********************************************************************************************
		//******* threshlod warpAffine image to 0 and 1 to create a heatmap table ***********************
		//******* applyColorMap by normalize heatmap table **********************************************
		//******* calculate occupy ratio ****************************************************************
		//***********************************************************************************************

		double min, max;
		cv::minMaxLoc(merge_thres, &min, &max);

		cout << "\n" << " ======== " << "\n";

		float img_2_total = img_2.total(), occupy;
		for (auto i = 1; i <= max; i++)
		{
			Mat colormap_thres;
			threshold(merge_thres, colormap_thres, i - 1, 1, THRESH_BINARY);
			//cout << "\n" << i << " img_2_total " << img_2_total << "\n";
			occupy = (countNonZero(colormap_thres) / img_2_total) * 100;
			cout << "\n" << i << " : occupy " << occupy << " %" << " -> occupy * 2  " << occupy * 2 << " %" << "\n";
		}

		if (!show_and_draw)
		{
			// Holds the colormap version of the image:
			Mat merge_thres_colormap;
			// Apply the colormap:
			cv::normalize(merge_thres, merge_thres, 0, 255, cv::NORM_MINMAX);
			applyColorMap(merge_thres, merge_thres_colormap, COLORMAP_JET);
			// Show the result:
			cv::imshow("merge_thres_colormap", merge_thres_colormap);
			cv::imwrite("merge_thres_colormap.bmp", merge);
			//cout << "\n" << merge_thres << "\n";
		}
	}
	else
	{
		cout << "\n" << "empty" << "\n";
	}

	cv::waitKey();
	std::system("pause");
	cv::destroyAllWindows();
	return 0;
}