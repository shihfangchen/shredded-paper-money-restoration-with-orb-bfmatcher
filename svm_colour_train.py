#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
from sklearn.svm import SVC
from skimage import exposure
from skimage import feature
from imutils import paths
import argparse
import imutils
import cv2
import numpy as np
import pickle
import os, random

ap = argparse.ArgumentParser()
ap.add_argument("-d", "--training", required=True, help="Path to the logos training dataset")
args = vars(ap.parse_args())

data = []
labels = []

for imagePath in paths.list_images(args["training"]):
    #print (np.reshape(H, (1, -1))) 
    make = imagePath.split("\\")[-2]
    
    image = cv2.imread(imagePath)
    HSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        
    H = cv2.calcHist([image],[0],None,[180],[0, 180])
    S = cv2.calcHist([image],[1],None,[255],[0, 255])    
    V = cv2.calcHist([image],[2],None,[255],[0, 255])
    H[0]=H[-1]=S[0]=S[-1]= V[0]=V[-1]=0
    HSV_con=np.concatenate((H, S, V), axis=0)
    HSV_con=np.reshape(HSV_con, (1, -1))[0]
        
    data.append(HSV_con)
    labels.append(make)

model = SVC(C=2.0, kernel='linear', degree=3, gamma='scale', coef0=0.0, shrinking=False, probability=True, tol=0.00001, cache_size=2000, 
class_weight='balanced', verbose=False, max_iter=-1, decision_function_shape='ovr', break_ties=True, random_state=10)
model.fit(data, labels)

results = model.predict_proba(data)[0]
prob_per_class_dictionary = dict(zip(model.classes_, results))
print (prob_per_class_dictionary)

with open('save/model.pickle', 'wb') as f:
    pickle.dump(model, f)