#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
from sklearn.svm import SVC
from skimage import exposure
from skimage import feature
from imutils import paths
import argparse
import imutils
import cv2
import numpy as np
import pickle
import os, random,sys
import pathlib
import shutil

def svm_classification(image,index):
    
    print (file_name)  
    #os.system("pause")
        
    #image = cv2.imread(imagePath)
    #cv2.namedWindow("ori Test Image",cv2.WINDOW_NORMAL)
    #cv2.imshow("ori Test Image", image)
    
    HSV = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)    
    H = cv2.calcHist([image],[0],None,[180],[0, 180])
    S = cv2.calcHist([image],[1],None,[255],[0, 255])    
    V = cv2.calcHist([image],[2],None,[255],[0, 255])
    H[0]=H[-1]=S[0]=S[-1]= V[0]=V[-1]=0
    HSV_con=np.concatenate((H, S, V), axis=0)
    HSV_con=np.reshape(HSV_con, (1, -1))[0]
        
    pred = model.predict(HSV_con.reshape(1, -1))[0]
 
    #os.system("pause")
    print ("\n",pred.title())
    #print (pred.title()+"\\"+file_name+"crop_colour_slice_mask"+str(index)+".bmp")

    cv2.imwrite(pred.title()+"\\"+file_name+"crop_colour_slice_mask"+str(index)+".bmp", image)   

    """
    cv2.putText(image, pred.title(), (0, 20), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), 3)
    cv2.namedWindow("Test Image",cv2.WINDOW_NORMAL)
    cv2.imshow("Test Image", image)
    cv2.waitKey(0) 
    """

def Bank_Notes_dominantColor_classification(image):
    colour_image = image.copy()
    h, w = image.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)
    mask[:] = 0
    cv2.floodFill(colour_image, mask,(0, 0), (255, 255, 255), (0,)*3, (0,)*3)
    cv2.floodFill(colour_image, mask,(w-1, h-1), (255, 255, 255), (0,)*3, (0,)*3)   
    gray = cv2.cvtColor(colour_image, cv2.COLOR_BGR2GRAY)
    empty, thres = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)     
    not_thres = ~thres;
    contours, hierarchy = cv2.findContours(not_thres, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)  
 
    for i in range(len(contours)):
        if len(contours[i]) > 100:
            canvas = np.zeros_like(image)
            cv2.drawContours(canvas, contours, i, (255,)*3, cv2.FILLED)
            contours_poly = cv2.approxPolyDP(contours[i],3,True)         
            boundRect = cv2.boundingRect(contours_poly)
            print (i,boundRect)
            if (boundRect[2]<5 or boundRect[3]<5):
                continue;
            slice_canvas = np.zeros_like(image)
            slice_canvas[boundRect[1]:boundRect[3]+boundRect[1],boundRect[0]:boundRect[2]+boundRect[0]] \
               = canvas[boundRect[1]:boundRect[3]+boundRect[1],boundRect[0]:boundRect[2]+boundRect[0]]
 
            colour_slice_mask = cv2.bitwise_and(colour_image,slice_canvas)
            crop_colour_slice_mask = colour_slice_mask[boundRect[1]:boundRect[3]+boundRect[1],boundRect[0]:boundRect[2]+boundRect[0]]         
            svm_classification(crop_colour_slice_mask,i)
            #cv2.namedWindow("crop_colour_slice_mask",cv2.WINDOW_NORMAL)  
            #cv2.imshow("crop_colour_slice_mask", crop_colour_slice_mask)  
            #cv2.waitKey()

try:
    os.makedirs("100")
    os.makedirs("500")
    os.makedirs("1000")
except : 
    shutil.rmtree("100")
    shutil.rmtree("500")
    shutil.rmtree("1000")
    os.makedirs("100")
    os.makedirs("500")
    os.makedirs("1000")

with open('save/model.pickle', 'rb') as f:
    model = pickle.load(f)

for imagePath in paths.list_images(sys.argv[1]):
    print (imagePath)
    file_name=imagePath.split("\\")[-1]
    src = cv2.imread(imagePath)
    #print (file_name)
    #cv2.imshow("src",src)
    #cv2.waitKey(0)
    #os.system("pause")

    Bank_Notes_dominantColor_classification(src)
    cv2.waitKey(0)
    cv2.destroyAllWindows()