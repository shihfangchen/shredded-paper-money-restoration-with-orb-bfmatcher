# Heterogeneous and Multitudinous Shredded Bank Notes Reassembly Approach

Heterogeneous and Multitudinous Shredded Bank Notes Reassembly Approach

Testing in Windows but I believe it can inference in Ubuntu system

OpenCV 4.0.1 with OpenCV contrib

# Demo Video

Look at Demo Video for more usage details

[Heterogeneous and Multitudinous Shredded Bank Notes Reassembly Approach](https://www.youtube.com/watch?v=BQn4OWszxgs&list=PLelD7IAZGUrHkbfnTiqnr_JriOGFqmmQ3&index=5)

for classification Shredded Bank Notes in photos:

`python Bank_Notes_dominantColor_classification.py argv[1]`

where 

argv[1] is the folder with Shredded Bank Notes' photos

the Shredded Bank Notes will be classified as 100 500 1000 to each folder  

for reassembly Shredded Bank Notes for each folder:

build Shred_Paper_Money_Res_with_ORB_BFM firstly, then, 

`./Shred_Paper_Money_Res_with_ORB_BFM argv[1] argv[2]`

where 

argv[1] is the folder name for reassembly

argv[2] is the refernce image

request 1. to show inference process, set 0 else 1

request 2. this is the scale_ratio , set this if the refernce image small / large then Shredded Bank Notes, set 1 in original scaling 

request 3. fastThreshold of ORB for ORB , you can set it as 3
